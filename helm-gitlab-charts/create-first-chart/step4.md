
# Most commonly used Helm commands

Now that we have installed Helm, let's us explore some of the most commonly used commands

## Helm Create

This command creates a chart directory along with the common files and directories used in a chart.

`helm create <chartname>`

## Search

Search provides the ability to search for Helm charts in the various places they can be stored including the Artifact Hub and repositories you have added. 

[helm search hub](https://helm.sh/docs/helm/helm_search_hub/) - search for charts in the Artifact Hub or your own hub instance

`helm search hub`{{execute}}

[helm search repo](https://helm.sh/docs/helm/helm_search_repo/) - search repositories for a keyword in charts

## Install redis from Artifact Hub

Create a namespace called `redis-ns`

`kubectl create namespace redis-ns`{{execute}}

Add the [bitmani repo](https://artifacthub.io/packages/helm/bitnami/nginx)

`helm repo add bitnami https://charts.bitnami.com/bitnami`{{execute}}

Once this completes install the chart

`helm install redis bitnami/redis -n redis-ns`{{execute}}

Confirm that the redis chart has been installed

`kubectl get all -n redis-ns`{{execute}}

## Check deployment status

This command shows the status of a named release.

`helm status -n nginx-ns nginx`{{execute}}

## Helm list

This command lists all of the releases for a specified namespace (uses current namespace context if namespace not specified).

`helm list -n nginx-ns`{{execute}}

## Helm Dependency

Manage the dependencies of a chart.

`helm dependency update /root/charts/first-chart`{{execute}}

## Helm Show

This command consists of multiple subcommands to display information about a chart.

`helm show all /root/charts/first-chart`{{execute}}

## Helm Upgrade

This command upgrades a release to a new version of a chart. After making changes to the chart, run

`helm upgrade -n nginx-ns nginx /root/charts/first-chart`{{execute}}

## Helm History

Prints historical revisions for a given release. To check deployment history from our previous chart

`helm history -n nginx-ns nginx`{{execute}}

## Helm Rollback

This command rolls back a release to a previous revision.

`helm rollback -n nginx-ns nginx`{{execute}}

## Update dependencies

`helm dependency update /root/charts/first-chart`{{execute}}

`tree /root/charts/first-chart`{{execute}}

## Inspect

`helm inspect all /root/charts/first-chart`{{execute}}

## Examine a chart for possible issues

`helm lint /root/charts/first-chart`{{execute}}

## Simulate installation

`helm upgrade -n nginx-ns nginx --debug --dry-run /root/charts/first-chart`{{execute}}

## Helm Uninstall

This command takes a release name and uninstalls the release.

`helm uninstall -n nginx-ns nginx`{{execute}}
