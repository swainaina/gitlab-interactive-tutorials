# Getting started with Helm

This tutorial covers how to get started using Helm and to create your first helm chart

## Prerequisites

The following prerequisites are required for a successful and properly secured use of Helm.

1. A Kubernetes cluster: You must have Kubernetes installed. For the latest release of Helm, we recommend the latest stable release of Kubernetes, which in most cases is the second-latest minor release.

1. You should also have a local configured copy of kubectl
