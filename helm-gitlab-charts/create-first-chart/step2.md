
# Create your first chart

Now that we have helm installed, let's create our first chart

Create a directory to store the charts

`mkdir charts && cd charts`{{execute}}

Use the helm command to create the chart

`helm create first-chart`{{execute}}

This command creates a chart directory along with the common files and directories used in a chart. By default Helm creates a nginx deployment
