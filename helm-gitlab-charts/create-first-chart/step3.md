
# Show the chart structure

`tree first-chart`{{execute}}

View the contents of the [Chart.yaml](https://helm.sh/docs/topics/charts/#the-chartyaml-file) file

`cat first-chart/Chart.yaml`{{execute}}

View the [values.yaml](https://helm.sh/docs/topics/charts/#templates-and-values) file

`cat first-chart/values.yaml`{{execute}}

Explore the contents of the [templates](https://helm.sh/docs/topics/charts/#templates-and-values) directory. All template files are stored in a chart's `templates/` folder. When Helm renders the charts, it will pass every file in that directory through the template engine.

`tree first-chart/templates`{{execute}}

The [charts/](https://helm.sh/docs/topics/charts/#managing-dependencies-manually-via-the-charts-directory) directory will house your subcharts or any  depenendicies that you have added

## Install the chart

Create a namespace called `nginx-ns`

`kubectl create namespace nginx-ns`{{execute}}

Now to install the chart

`helm install nginx -n nginx-ns first-chart/`{{execute}}

Confirm that the deployment has been installed

`kubectl get all -n nginx-ns`{{execute}}
