# Installing Gitlab using Helm

This tutorial covers how to install GitLab using helm

## Prerequisites

The following prerequisites are required for a successful and properly secured use of Helm.

1. We will be using minikube to install
