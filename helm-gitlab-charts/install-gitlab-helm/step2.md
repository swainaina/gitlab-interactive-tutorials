# Install kind

curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.0/kind-linux-amd64
chmod +x ./kind
mv ./kind /some-dir-in-your-PATH/kind


wget https://gitlab.com/gitlab-org/charts/gitlab/-/raw/master/scripts/gke_bootstrap_script.sh

apt install python3

curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-342.0.0-linux-x86_64.tar.gz

tar -xvf google-cloud-sdk-342.0.0-linux-x86_64.tar.gz

gcloud container clusters get-credentials <cluster-name> --zone <zone> --project <project-id>