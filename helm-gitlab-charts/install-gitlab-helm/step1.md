# Installing Helm on Linux

In this step we will install Helm.

There are several options when it comes to [installing Helm](https://helm.sh/docs/intro/install/). In our case we will be using the community package [Apt (Debian/Ubuntu)](https://helm.sh/docs/intro/install/#from-apt-debianubuntu) to install.

`curl https://baltocdn.com/helm/signing.asc | sudo apt-key add - && sudo apt-get install apt-transport-https --yes && echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list && sudo apt-get update && sudo apt-get install helm`{{execute}}

After installation, check that Helm was installed successfully

`helm version`{{execute}}

To get help run the following command

`helm --help`{{execute}}